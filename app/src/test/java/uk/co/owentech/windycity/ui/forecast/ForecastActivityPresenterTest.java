package uk.co.owentech.windycity.ui.forecast;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import uk.co.owentech.windycity.data.TestSchedulerRule;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;
import uk.co.owentech.windycity.model.ForecastResponse;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ForecastActivityPresenterTest {

    @Mock
    WindyService service;
    @Mock
    ForecastActivityViewContract viewContract;

    ForecastActivityPresenter presenter;

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        presenter = new ForecastActivityPresenter();
        presenter.service = service;
        presenter.viewContract = viewContract;
    }

    @Test
    public void getForecast_success() throws Exception {
        ForecastResponse forecastResponse = new ForecastResponse();
        Single<Result<ForecastResponse>> single = Single.just(Result.response(Response.success(forecastResponse)));
        when(service.getForcastForCityId(anyLong())).thenReturn(single);
        presenter.getForecast(anyLong());
        testSchedulerRule.getTestScheduler().triggerActions();
        verify(viewContract).forecastSuccess(eq(forecastResponse));
    }

    @Test
    public void getForecast_error() throws Exception {
        ResponseBody responseBody = mock(ResponseBody.class);
        Single single = Single.just(Result.response(Response.error(404, responseBody)));
        when(service.getForcastForCityId(anyLong())).thenReturn(single);
        presenter.getForecast(anyLong());
        testSchedulerRule.getTestScheduler().triggerActions();
        verify(viewContract).forecastError();
    }

}