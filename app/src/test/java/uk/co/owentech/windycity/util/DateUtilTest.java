package uk.co.owentech.windycity.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class DateUtilTest {
    @Test
    public void dateFromEpoch() throws Exception {
        assertEquals("10 Feb 2017 - 12:00", DateUtil.dateFromEpoch(1486728000L));
    }

}