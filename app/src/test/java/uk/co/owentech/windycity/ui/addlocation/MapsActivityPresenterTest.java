package uk.co.owentech.windycity.ui.addlocation;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import dagger.Module;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import uk.co.owentech.windycity.data.StorageHelper;
import uk.co.owentech.windycity.data.TestSchedulerRule;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class MapsActivityPresenterTest {

    MapsActivityPresenter presenter;
    @Mock
    WindyService service;
    @Mock
    MapsActivityViewContract viewContract;
    @Mock
    StorageHelper storageHelper;

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        presenter = new MapsActivityPresenter();
        presenter.service = service;
        presenter.viewContract = viewContract;
        presenter.storageHelper = storageHelper;
    }

    @Test
    public void searchWeatherByLatLng_success() throws Exception {
        CurrentWeatherResponse currentWeatherResponse = new CurrentWeatherResponse();
        currentWeatherResponse.setCityId(100L);
        Single<Result<CurrentWeatherResponse>> single = Single.just(Result.response(Response.success(currentWeatherResponse)));
        when(service.getCurrentWeatherForLatLng(anyDouble(), anyDouble())).thenReturn(single);
        presenter.searchWeatherByLatLng(1d, 2d);
        testSchedulerRule.getTestScheduler().triggerActions();
        ArgumentCaptor<FavouriteCity> argumentCaptor = ArgumentCaptor.forClass(FavouriteCity.class);
        verify(storageHelper).addFavouriteCity(argumentCaptor.capture());
        assertTrue(argumentCaptor.getValue().getCityId() == 100L);
        verify(viewContract).searchSuccess(eq(currentWeatherResponse));

    }

    @Test
    public void searchWeatherByLatLng_response_error() throws Exception {
        ResponseBody responseBody = mock(ResponseBody.class);
        Single single = Single.just(Result.response(Response.error(404, responseBody)));
        when(service.getCurrentWeatherForLatLng(anyDouble(), anyDouble())).thenReturn(single);
        presenter.searchWeatherByLatLng(1d, 2d);
        testSchedulerRule.getTestScheduler().triggerActions();
        verify(viewContract).searchError();
    }
}