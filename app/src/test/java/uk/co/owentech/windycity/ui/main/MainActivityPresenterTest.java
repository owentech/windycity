package uk.co.owentech.windycity.ui.main;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashMap;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import uk.co.owentech.windycity.data.StorageHelper;
import uk.co.owentech.windycity.data.TestSchedulerRule;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class MainActivityPresenterTest {

    @Mock
    WindyService service;
    @Mock
    StorageHelper storageHelper;
    @Mock
    MainActivityViewContract viewContract;

    MainActivityPresenter presenter;

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        presenter = new MainActivityPresenter();
        presenter.service = service;
        presenter.storageHelper = storageHelper;
        presenter.viewContract = viewContract;
    }

    @Test
    public void getFavouriteCities_null() throws Exception {
        when(storageHelper.getStoredCities()).thenReturn(null);
        presenter.getFavouriteCities();
        verify(viewContract, times(1)).noFavouriteCities();
    }

    @Test
    public void getFavouriteCities_no_cities() throws Exception {
        LinkedHashMap<Long, FavouriteCity> map = mock(LinkedHashMap.class);
        when(storageHelper.getStoredCities()).thenReturn(map);
        presenter.getFavouriteCities();
        verify(viewContract, times(1)).noFavouriteCities();
    }

    @Test
    public void getFavouriteCities_cities_available() throws Exception {
        LinkedHashMap<Long, FavouriteCity> map = mock(LinkedHashMap.class);
        when(map.size()).thenReturn(1);
        when(storageHelper.getStoredCities()).thenReturn(map);
        presenter.getFavouriteCities();
        verify(viewContract, times(1)).updateFavouriteCities(eq(map));
    }

    @Test
    public void updateWeatherForCityId_success() throws Exception {
        CurrentWeatherResponse currentWeatherResponse = mock(CurrentWeatherResponse.class);
        Single<Result<CurrentWeatherResponse>> single = Single.just(Result.response(Response.success(currentWeatherResponse)));
        when(service.getCurrentWeatherForCityId(anyLong())).thenReturn(single);
        FavouriteCity favouriteCity = mock(FavouriteCity.class);

        presenter.updateWeatherForCity(favouriteCity);
        testSchedulerRule.getTestScheduler().triggerActions();
        verify(storageHelper).updateCity(any(FavouriteCity.class));
        verify(viewContract).weatherUpdated();
    }

    @Test
    public void updateWeatherForCityId_error() throws Exception {
        ResponseBody responseBody = mock(ResponseBody.class);
        Single single = Single.just(Result.response(Response.error(404, responseBody)));
        when(service.getCurrentWeatherForCityId(anyLong())).thenReturn(single);
        FavouriteCity favouriteCity = mock(FavouriteCity.class);
        presenter.updateWeatherForCity(favouriteCity);
        testSchedulerRule.getTestScheduler().triggerActions();
        verify(viewContract).weatherUpdateFailed();
    }

    @Test
    public void removeFavouriteCity() throws Exception {
        FavouriteCity favouriteCity = mock(FavouriteCity.class);
        presenter.removeFavouriteCity(favouriteCity);
        verify(storageHelper).removeFavouriteCity(eq(favouriteCity));
    }
}