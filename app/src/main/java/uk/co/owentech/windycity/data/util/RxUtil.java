package uk.co.owentech.windycity.data.util;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RxUtil {

    public static <T> Single<NetworkResult<T>> networkTask(Single<Result<T>> single){
        return single.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Result<T>, NetworkResult<T>>() {
                    @Override
                    public NetworkResult<T> apply(Result<T> result) throws Exception {
                        if (result.response() != null) {
                            if (result.response().isSuccessful()) {
                                return mapResult(result);
                            } else {
                                return networkResultForResponseError(result);
                            }
                        }
                        return networkResultForThrowable(result.error());
                    }
                })
                .onErrorReturn(new Function<Throwable, NetworkResult<T>>() {
                    @Override
                    public NetworkResult<T> apply(Throwable throwable) throws Exception {
                        return networkResultForThrowable(throwable);
                    }
                });
    }

    private static <T> NetworkResult<T> mapResult(Result<T> result) {
        NetworkResult<T> networkResult = new NetworkResult<>();
        networkResult.setSuccess(true);
        networkResult.setResultObject(result.response().body());
        return networkResult;
    }

    private static <T> NetworkResult<T> networkResultForResponseError(Result<T> result) {
        NetworkResult<T> networkResult = new NetworkResult<>();
        networkResult.setSuccess(false);
        if (result.response() != null) {
            networkResult.setErrorCode(result.response().code());
        }
        return networkResult;
    }

    private static <T> NetworkResult<T> networkResultForThrowable(Throwable throwable) {
        NetworkResult<T> networkResult = new NetworkResult<>();
        networkResult.setSuccess(false);
        networkResult.setThrowable(throwable);
        return networkResult;
    }

}
