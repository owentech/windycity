package uk.co.owentech.windycity.ui.forecast;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.owentech.windycity.R;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.ForecastResponse;
import uk.co.owentech.windycity.util.DateUtil;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private ForecastResponse forecastResponse = new ForecastResponse();

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_forecast, parent, false);
        ForecastViewHolder viewHolder = new ForecastViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        ForecastResponse.Forecast forecast = forecastResponse.getList().get(position);
        if (forecast.getDate() != null){
            holder.dateTextView.setText(DateUtil.dateFromEpoch(forecast.getDate()));
        }
        if (forecast.getWind().deg != null) {
            holder.windArrorImage.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
            holder.windArrorImage.setRotation((float) (360 - forecast.getWind().deg));
        }
        else{
            holder.windArrorImage.setImageResource(R.drawable.ic_help_outline_black_24dp);
            holder.windArrorImage.setRotation(0);
        }
        holder.windSpeedTextView.setText(speedAndDirectionText(forecast));
    }

    @Override
    public int getItemCount() {
        return forecastResponse.getList().size();
    }

    public void setForecastResponse(ForecastResponse forecastResponse) {
        this.forecastResponse = forecastResponse;
        notifyDataSetChanged();
    }

    private String speedAndDirectionText(ForecastResponse.Forecast forecast){
        if (forecast.getWind().deg != null){
            return String.format(Locale.getDefault(), "%.1fmph - %.0f°", forecast.getWind().speed, forecast.getWind().deg);
        }
        return String.format(Locale.getDefault(), "%.1fmph", forecast.getWind().speed);
    }

    static class ForecastViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dateTextView) TextView dateTextView;
        @BindView(R.id.windSpeedTextView) TextView windSpeedTextView;
        @BindView(R.id.windArrowImage) ImageView windArrorImage;

        public ForecastViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
