package uk.co.owentech.windycity.data;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.plugins.RxJavaPlugins;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.ForecastResponse;

@Singleton
public class WindyService {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    WeatherService weatherService;

    @Inject
    public WindyService(){}

    public interface WeatherService {
        @GET("weather")
        Single<Result<CurrentWeatherResponse>> currentWeatherByLatLng(@Query("lat") Double lat, @Query("lon") Double lon);

        @GET("weather")
        Single<Result<CurrentWeatherResponse>> currentWeatherById(@Query("id") Long id);

        @GET("forecast")
        Single<Result<ForecastResponse>> forcastById(@Query("id") Long id);
    }

    private WeatherService getWeatherService(){
        if (weatherService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(WindyApplication.getInstance().getApplicationComponent().getOKHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
            weatherService = retrofit.create(WeatherService.class);
        }
        return weatherService;
    }

    public Single<Result<CurrentWeatherResponse>> getCurrentWeatherForLatLng(Double lat, Double lon){
        return getWeatherService().currentWeatherByLatLng(lat, lon);
    }

    public Single<Result<CurrentWeatherResponse>> getCurrentWeatherForCityId(Long cityId) {
        return getWeatherService().currentWeatherById(cityId);
    }

    public Single<Result<ForecastResponse>> getForcastForCityId(Long cityId) {
        return getWeatherService().forcastById(cityId);
    }
}
