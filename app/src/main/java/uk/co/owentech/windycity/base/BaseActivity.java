package uk.co.owentech.windycity.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    @Inject
    T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateDependencies();
    }

    public void updateDependencies(){

    }

    public T getPresenter(){
        return presenter;
    }
}
