package uk.co.owentech.windycity;

import android.app.Application;

import io.paperdb.Paper;
import uk.co.owentech.windycity.di.application.ApplicationComponent;
import uk.co.owentech.windycity.di.application.ApplicationModule;
import uk.co.owentech.windycity.di.application.DaggerApplicationComponent;

public class WindyApplication extends Application {

    private static WindyApplication instance;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        WindyApplication.instance = this;
        Paper.init(this);

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule())
                .build();
        applicationComponent.inject(this);
    }

    public static WindyApplication getInstance() {
        return instance;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
