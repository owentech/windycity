package uk.co.owentech.windycity.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    public static String dateFromEpoch(Long epochDate) {
        Date date = new Date();
        date.setTime(epochDate * 1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy - hh:mm", Locale.getDefault());
        return simpleDateFormat.format(date);
    }
}
