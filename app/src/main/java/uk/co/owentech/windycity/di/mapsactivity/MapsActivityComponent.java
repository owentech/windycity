package uk.co.owentech.windycity.di.mapsactivity;

import dagger.Component;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.di.application.ApplicationComponent;
import uk.co.owentech.windycity.ui.addlocation.MapsActivity;

@PerActivity
@Component(modules = {MapsActivityModule.class}, dependencies = ApplicationComponent.class)
public interface MapsActivityComponent {
    void inject(MapsActivity activity);
}
