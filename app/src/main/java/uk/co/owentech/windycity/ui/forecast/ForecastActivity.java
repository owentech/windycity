package uk.co.owentech.windycity.ui.forecast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.owentech.windycity.R;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.base.BaseActivity;
import uk.co.owentech.windycity.di.forecastactivity.DaggerForecastActivityComponent;
import uk.co.owentech.windycity.di.forecastactivity.ForecastActivityModule;
import uk.co.owentech.windycity.model.FavouriteCity;
import uk.co.owentech.windycity.model.ForecastResponse;

public class ForecastActivity extends BaseActivity<ForecastActivityPresenter> implements ForecastActivityViewContract{

    public static final String FAV_CITY = "FavouriteCity";

    ForecastAdapter forecastAdapter;
    FavouriteCity favouriteCity;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setupView();
        refreshLayout.setRefreshing(true);

        getPresenter().getForecast(favouriteCity.getCityId());
    }

    private void setupView(){
        favouriteCity = (FavouriteCity)getIntent().getSerializableExtra(FAV_CITY);
        setTitle(favouriteCity.getName());
        forecastAdapter = new ForecastAdapter();
        recyclerView.setAdapter(forecastAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        refreshLayout.setEnabled(false);
    }

    @Override
    public void updateDependencies() {
        DaggerForecastActivityComponent.builder()
                .applicationComponent(WindyApplication.getInstance().getApplicationComponent())
                .forecastActivityModule(new ForecastActivityModule(this))
                .build().inject(this);
    }

    @Override
    public void forecastSuccess(ForecastResponse forecastResponse) {
        refreshLayout.setRefreshing(false);
        forecastAdapter.setForecastResponse(forecastResponse);
    }

    @Override
    public void forecastError() {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
    }
}
