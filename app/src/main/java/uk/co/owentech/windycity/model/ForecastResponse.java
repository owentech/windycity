package uk.co.owentech.windycity.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ForecastResponse {

    private List<Forecast> list = new ArrayList<>();

    public static class Forecast {
        @SerializedName("dt")
        private Long date;
        private Wind wind;

        public static class Wind {
            public Double speed;
            public Double deg;
        }

        public Long getDate() {
            return date;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public Wind getWind() {
            return wind;
        }

        public void setWind(Wind wind) {
            this.wind = wind;
        }
    }

    public List<Forecast> getList() {
        return list;
    }

    public void setList(List<Forecast> list) {
        this.list = list;
    }
}
