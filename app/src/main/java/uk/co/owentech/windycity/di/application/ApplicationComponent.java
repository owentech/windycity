package uk.co.owentech.windycity.di.application;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.data.StorageHelper;
import uk.co.owentech.windycity.data.WindyService;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(WindyApplication application);

    WindyService getWindyService();
    StorageHelper getStorageHelper();
    OkHttpClient getOKHttpClient();
}
