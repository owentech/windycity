package uk.co.owentech.windycity.di.forecastactivity;

import dagger.Module;
import dagger.Provides;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.ui.forecast.ForecastActivityViewContract;

@Module
public class ForecastActivityModule {

    private ForecastActivityViewContract contract;

    public ForecastActivityModule(ForecastActivityViewContract contract){
        this.contract = contract;
    }

    @Provides
    @PerActivity
    ForecastActivityViewContract provideForecastActivityViewContract(){
        return contract;
    }

}
