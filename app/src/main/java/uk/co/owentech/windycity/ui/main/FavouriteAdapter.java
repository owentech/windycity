package uk.co.owentech.windycity.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.owentech.windycity.R;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;
import uk.co.owentech.windycity.util.DateUtil;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>{

    public static final int ITEM_TYPE_CONTENT = 1;
    public static final int ITEM_TYPE_NO_CONTENT = 2;

    List<FavouriteCity> favouriteCityList = new ArrayList<>();
    MainActivityViewContract mainActivityViewContract;

    public FavouriteAdapter(MainActivityViewContract contract) {
        mainActivityViewContract = contract;
    }

    @Override
    public FavouriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_favourite_city, parent, false);
        FavouriteViewHolder viewHolder = new FavouriteViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FavouriteViewHolder holder, final int position) {
        if (getItemViewType(position) == ITEM_TYPE_CONTENT) {
            setupContentView(holder, position);
        }
        else{
            setupNoContentView(holder);
        }
    }

    @Override
    public int getItemCount() {
        if (favouriteCityList.size() != 0) {
            return favouriteCityList.size();
        }
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (favouriteCityList.size() != 0){
            return ITEM_TYPE_CONTENT;
        }
        return ITEM_TYPE_NO_CONTENT;
    }

    private void setupContentView(final FavouriteViewHolder holder, final int position){
        FavouriteCity favouriteCity = favouriteCityList.get(position);
        holder.contentLayout.setVisibility(View.VISIBLE);
        holder.noContentTextView.setVisibility(View.GONE);
        holder.cityNameTextView.setText(favouriteCity.getName());
        holder.windSpeedTextView.setText(speedAndDirectionText(favouriteCity.getCurrentWeatherResponse()));
        if (favouriteCity.getCurrentWeatherResponse().getWind().getDeg() != null) {
            holder.windArrorImage.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
            holder.windArrorImage.setRotation((float) (360 - favouriteCity.getCurrentWeatherResponse().getWind().getDeg()));
        }
        else{
            holder.windArrorImage.setImageResource(R.drawable.ic_help_outline_black_24dp);
            holder.windArrorImage.setRotation(0);
        }
        if (favouriteCity.getCurrentWeatherResponse().getDate() != null){
            holder.dateTextView.setText(getDateText(favouriteCity.getCurrentWeatherResponse().getDate()));
        }
    }

    private String getDateText(Long epochDate){
        return WindyApplication.getInstance().getResources().getString(R.string.last_updated, DateUtil.dateFromEpoch(epochDate));
    }

    private void setupNoContentView(final FavouriteViewHolder holder){
        holder.contentLayout.setVisibility(View.GONE);
        holder.noContentTextView.setVisibility(View.VISIBLE);
    }

    public void setFavouriteCityList(List<FavouriteCity> favouriteCityList) {
        this.favouriteCityList = favouriteCityList;
        notifyDataSetChanged();
    }

    public List<FavouriteCity> getFavouriteCityList() {
        return favouriteCityList;
    }

    private String speedAndDirectionText(CurrentWeatherResponse currentWeatherResponse){
        if (currentWeatherResponse.getWind().getDeg() != null){
            return String.format(Locale.getDefault(), "%.1fmph - %.0f°", currentWeatherResponse.getWind().getSpeed(), currentWeatherResponse.getWind().getDeg());
        }
        return String.format(Locale.getDefault(), "%.1fmph", currentWeatherResponse.getWind().getSpeed());
    }

    class FavouriteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.contentLayout) RelativeLayout contentLayout;
        @BindView(R.id.noContentTextView) TextView noContentTextView;
        @BindView(R.id.cityNameTextView) TextView cityNameTextView;
        @BindView(R.id.windSpeedTextView) TextView windSpeedTextView;
        @BindView(R.id.windArrowImage) ImageView windArrorImage;
        @BindView(R.id.dateTextView) TextView dateTextView;
        @BindView(R.id.fiveDayButton) Button fiveDayButton;
        @BindView(R.id.refreshButton) Button refreshButton;

        public FavouriteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            refreshButton.setOnClickListener(this);
            fiveDayButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.refreshButton){
                if (mainActivityViewContract != null) {
                    mainActivityViewContract.refreshButtonClicked(this.getAdapterPosition());
                }
            }
            else {
                if (mainActivityViewContract != null) {
                    mainActivityViewContract.fiveDayButtonClicked(this.getAdapterPosition());
                }
            }

        }
    }


}
