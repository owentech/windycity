package uk.co.owentech.windycity.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.LongSparseArray;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.owentech.windycity.R;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.base.BaseActivity;
import uk.co.owentech.windycity.di.mainactivity.DaggerMainActivityComponent;
import uk.co.owentech.windycity.di.mainactivity.MainActivityModule;
import uk.co.owentech.windycity.model.FavouriteCity;
import uk.co.owentech.windycity.ui.addlocation.MapsActivity;
import uk.co.owentech.windycity.ui.forecast.ForecastActivity;

public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityViewContract {

    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    FavouriteAdapter favouriteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupView();
        getPresenter().getFavouriteCities();
    }

    private void setupView(){
        setSupportActionBar(toolbar);
        refreshLayout.setEnabled(false);
        favouriteAdapter = new FavouriteAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(favouriteAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivityForResult(intent, MapsActivity.REQUEST_CODE_MAPS);
            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                getPresenter().removeFavouriteCity(favouriteAdapter.getFavouriteCityList().get(viewHolder.getAdapterPosition()));
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void updateDependencies() {
        DaggerMainActivityComponent.builder()
                .applicationComponent(WindyApplication.getInstance().getApplicationComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build().inject(this);
    }


    @Override
    public void noFavouriteCities() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void updateFavouriteCities(LinkedHashMap<Long, FavouriteCity> favouriteCities) {
        refreshLayout.setRefreshing(false);
        if (favouriteAdapter != null){
            favouriteAdapter.setFavouriteCityList(new ArrayList<>(favouriteCities.values()));
        }
    }

    @Override
    public void weatherUpdated() {
        getPresenter().getFavouriteCities();
    }

    @Override
    public void weatherUpdateFailed() {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MapsActivity.REQUEST_CODE_MAPS && resultCode == RESULT_OK){
            Toast.makeText(this, getResources().getString(R.string.added_location_message, data.getStringExtra(MapsActivity.LOCATION_NAME)), Toast.LENGTH_SHORT).show();
            getPresenter().getFavouriteCities();
        }
    }

    @Override
    public void refreshButtonClicked(int position) {
        refreshLayout.setRefreshing(true);
        getPresenter().updateWeatherForCity(favouriteAdapter.getFavouriteCityList().get(position));
    }

    @Override
    public void fiveDayButtonClicked(int position) {
        Intent intent = new Intent(this, ForecastActivity.class);
        intent.putExtra(ForecastActivity.FAV_CITY, favouriteAdapter.getFavouriteCityList().get(position));
        startActivity(intent);
    }
}
