package uk.co.owentech.windycity.ui.addlocation;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.owentech.windycity.R;
import uk.co.owentech.windycity.WindyApplication;
import uk.co.owentech.windycity.base.BaseActivity;
import uk.co.owentech.windycity.di.mapsactivity.DaggerMapsActivityComponent;
import uk.co.owentech.windycity.di.mapsactivity.MapsActivityModule;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;

public class MapsActivity extends BaseActivity<MapsActivityPresenter> implements MapsActivityViewContract, OnMapReadyCallback {

    public static final int REQUEST_CODE_MAPS = 1;
    public static final String LOCATION_NAME = "LocationName";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.refreshLayout) SwipeRefreshLayout refreshLayout;
    private GoogleMap mMap;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        refreshLayout.setEnabled(false);
    }

    @Override
    public void updateDependencies() {
        DaggerMapsActivityComponent.builder()
                .applicationComponent(WindyApplication.getInstance().getApplicationComponent())
                .mapsActivityModule(new MapsActivityModule(this))
                .build().inject(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (marker != null){
                    marker.remove();
                }
                LatLng clickedLocation = new LatLng(latLng.latitude, latLng.longitude);
                marker = mMap.addMarker(new MarkerOptions().position(clickedLocation));

                mMap.moveCamera(CameraUpdateFactory.newLatLng(clickedLocation));
                fab.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.fab)
    void fabClicked(){
        refreshLayout.setRefreshing(true);
        getPresenter().searchWeatherByLatLng(marker.getPosition().latitude, marker.getPosition().longitude);
    }

    @Override
    public void searchSuccess(CurrentWeatherResponse response) {
        refreshLayout.setRefreshing(false);
        Intent intent = new Intent();
        intent.putExtra(LOCATION_NAME, response.getName());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void searchError() {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
    }
}
