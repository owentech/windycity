package uk.co.owentech.windycity.ui.main;

import android.util.LongSparseArray;

import java.util.LinkedHashMap;
import java.util.List;

import uk.co.owentech.windycity.model.FavouriteCity;

public interface MainActivityViewContract {
    void noFavouriteCities();
    void updateFavouriteCities(LinkedHashMap<Long, FavouriteCity> favouriteCities);
    void weatherUpdated();
    void weatherUpdateFailed();
    void refreshButtonClicked(int position);
    void fiveDayButtonClicked(int position);
}
