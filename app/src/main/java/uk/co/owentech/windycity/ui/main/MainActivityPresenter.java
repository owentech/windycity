package uk.co.owentech.windycity.ui.main;

import android.util.LongSparseArray;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import uk.co.owentech.windycity.base.BasePresenter;
import uk.co.owentech.windycity.data.StorageHelper;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.data.util.NetworkResult;
import uk.co.owentech.windycity.data.util.RxUtil;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;
import uk.co.owentech.windycity.model.ForecastResponse;

public class MainActivityPresenter implements BasePresenter<MainActivityViewContract> {

    @Inject
    StorageHelper storageHelper;
    @Inject
    WindyService service;
    @Inject
    MainActivityViewContract viewContract;

    @Inject
    MainActivityPresenter() {
    }

    public void getFavouriteCities() {
        LinkedHashMap<Long, FavouriteCity> favouriteCities = storageHelper.getStoredCities();
        if (favouriteCities == null || favouriteCities.size() == 0) {
            viewContract.noFavouriteCities();
        } else {
            viewContract.updateFavouriteCities(favouriteCities);
        }
    }

    public void updateWeatherForCity(final FavouriteCity city) {
        RxUtil.networkTask(service.getCurrentWeatherForCityId(city.getCityId()))
                .subscribe(new Consumer<NetworkResult<CurrentWeatherResponse>>() {
                    @Override
                    public void accept(NetworkResult<CurrentWeatherResponse> result) throws Exception {
                        if (result.getSuccess()) {
                            FavouriteCity updatedFavouriteCity = city;
                            updatedFavouriteCity.setCurrentWeatherResponse(result.getResultObject());
                            storageHelper.updateCity(updatedFavouriteCity);
                            viewContract.weatherUpdated();
                        } else {
                            viewContract.weatherUpdateFailed();
                        }
                    }
                });
    }

    public void removeFavouriteCity(final FavouriteCity city) {
        viewContract.updateFavouriteCities(storageHelper.removeFavouriteCity(city));
    }
}
