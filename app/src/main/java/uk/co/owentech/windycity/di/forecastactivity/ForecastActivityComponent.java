package uk.co.owentech.windycity.di.forecastactivity;

import dagger.Component;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.di.application.ApplicationComponent;
import uk.co.owentech.windycity.ui.forecast.ForecastActivity;

@PerActivity
@Component(modules = {ForecastActivityModule.class}, dependencies = ApplicationComponent.class)
public interface ForecastActivityComponent {
    void inject(ForecastActivity activity);
}
