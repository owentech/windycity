package uk.co.owentech.windycity.ui.forecast;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import uk.co.owentech.windycity.base.BasePresenter;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.data.util.NetworkResult;
import uk.co.owentech.windycity.data.util.RxUtil;
import uk.co.owentech.windycity.model.ForecastResponse;


public class ForecastActivityPresenter implements BasePresenter<ForecastActivityViewContract> {

    @Inject
    WindyService service;
    @Inject
    ForecastActivityViewContract viewContract;

    @Inject
    ForecastActivityPresenter(){}

    public void getForecast(Long cityId){
        RxUtil.networkTask(service.getForcastForCityId(cityId))
                .subscribe(new Consumer<NetworkResult<ForecastResponse>>() {
                    @Override
                    public void accept(NetworkResult<ForecastResponse> result) throws Exception {
                        if (result.getSuccess()){
                            viewContract.forecastSuccess(result.getResultObject());
                        }
                        else{
                            viewContract.forecastError();
                        }
                    }
                });
    }
}
