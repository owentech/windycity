package uk.co.owentech.windycity.ui.forecast;

import uk.co.owentech.windycity.model.ForecastResponse;

public interface ForecastActivityViewContract {
    void forecastSuccess(ForecastResponse forecastResponse);
    void forecastError();
}
