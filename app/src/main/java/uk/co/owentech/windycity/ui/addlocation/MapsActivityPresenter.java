package uk.co.owentech.windycity.ui.addlocation;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import uk.co.owentech.windycity.base.BasePresenter;
import uk.co.owentech.windycity.data.StorageHelper;
import uk.co.owentech.windycity.data.WindyService;
import uk.co.owentech.windycity.data.util.NetworkResult;
import uk.co.owentech.windycity.data.util.RxUtil;
import uk.co.owentech.windycity.model.CurrentWeatherResponse;
import uk.co.owentech.windycity.model.FavouriteCity;

public class MapsActivityPresenter implements BasePresenter<MapsActivityViewContract> {

    @Inject
    WindyService service;
    @Inject
    StorageHelper storageHelper;
    @Inject
    MapsActivityViewContract viewContract;

    @Inject
    MapsActivityPresenter() {
    }

    public void searchWeatherByLatLng(double lat, double lng) {
        RxUtil.networkTask(service.getCurrentWeatherForLatLng(lat, lng))
                .subscribe(new Consumer<NetworkResult<CurrentWeatherResponse>>() {
                    @Override
                    public void accept(NetworkResult<CurrentWeatherResponse> result) throws Exception {
                        if (result.getSuccess()) {
                            storageHelper.addFavouriteCity(favouriteCityFromWeatherResponse(result.getResultObject()));
                            viewContract.searchSuccess(result.getResultObject());
                        } else {
                            if (result.getThrowable() != null){
                                result.getThrowable().printStackTrace();
                            }
                            viewContract.searchError();
                        }
                    }
                });
    }

    private FavouriteCity favouriteCityFromWeatherResponse(CurrentWeatherResponse response) {
        FavouriteCity favouriteCity = new FavouriteCity();
        favouriteCity.setCityId(response.getCityId());
        favouriteCity.setName(response.getName());
        favouriteCity.setCurrentWeatherResponse(response);
        return favouriteCity;
    }

}
