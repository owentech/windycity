package uk.co.owentech.windycity.di.mapsactivity;

import dagger.Module;
import dagger.Provides;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.ui.addlocation.MapsActivityViewContract;

@Module
public class MapsActivityModule {

    private MapsActivityViewContract contract;

    public MapsActivityModule(MapsActivityViewContract contract){
        this.contract = contract;
    }

    @Provides
    @PerActivity
    MapsActivityViewContract provideMapsActivityViewContract(){
        return contract;
    }
}
