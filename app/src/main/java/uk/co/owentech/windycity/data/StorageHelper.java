package uk.co.owentech.windycity.data;

import java.util.LinkedHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.paperdb.Paper;
import uk.co.owentech.windycity.model.FavouriteCity;

@Singleton
public class StorageHelper {

    public static final String KEY_FAVOURITE_CITIES = "FavouriteCities";

    @Inject
    public StorageHelper(){}

    public LinkedHashMap<Long, FavouriteCity> getStoredCities(){
        return Paper.book().read(KEY_FAVOURITE_CITIES, new LinkedHashMap<Long, FavouriteCity>());
    }

    public void addFavouriteCity(final FavouriteCity favouriteCity){
        LinkedHashMap<Long, FavouriteCity> favouriteCities = Paper.book().read(KEY_FAVOURITE_CITIES, new LinkedHashMap<Long, FavouriteCity>());
        favouriteCities.put(favouriteCity.getCityId(), favouriteCity);
        Paper.book().write(KEY_FAVOURITE_CITIES, favouriteCities);
    }

    public void updateCity(final FavouriteCity favouriteCity){
        LinkedHashMap<Long, FavouriteCity> favouriteCities = Paper.book().read(KEY_FAVOURITE_CITIES, new LinkedHashMap<Long, FavouriteCity>());
        favouriteCities.put(favouriteCity.getCityId(), favouriteCity);
        Paper.book().write(KEY_FAVOURITE_CITIES, favouriteCities);
    }

    public LinkedHashMap<Long, FavouriteCity> removeFavouriteCity(final FavouriteCity favouriteCity) {
        LinkedHashMap<Long, FavouriteCity> favouriteCities = Paper.book().read(KEY_FAVOURITE_CITIES, new LinkedHashMap<Long, FavouriteCity>());
        favouriteCities.remove(favouriteCity.getCityId());
        Paper.book().write(KEY_FAVOURITE_CITIES, favouriteCities);
        return favouriteCities;
    }
}
