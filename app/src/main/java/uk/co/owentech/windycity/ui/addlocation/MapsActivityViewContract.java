package uk.co.owentech.windycity.ui.addlocation;

import uk.co.owentech.windycity.model.CurrentWeatherResponse;

public interface MapsActivityViewContract {
    void searchSuccess(CurrentWeatherResponse response);
    void searchError();
}
