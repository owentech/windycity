package uk.co.owentech.windycity.model;

import java.io.Serializable;

public class FavouriteCity implements Serializable {

    private Long cityId;
    private String name;
    private Long lastUpdated;
    private CurrentWeatherResponse currentWeatherResponse;

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public CurrentWeatherResponse getCurrentWeatherResponse() {
        return currentWeatherResponse;
    }

    public void setCurrentWeatherResponse(CurrentWeatherResponse currentWeatherResponse) {
        this.currentWeatherResponse = currentWeatherResponse;
    }
}
