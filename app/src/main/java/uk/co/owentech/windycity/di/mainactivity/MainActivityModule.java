package uk.co.owentech.windycity.di.mainactivity;

import dagger.Module;
import dagger.Provides;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.ui.main.MainActivityViewContract;

@Module
public class MainActivityModule {

    private MainActivityViewContract contract;

    public MainActivityModule(MainActivityViewContract contract){
        this.contract = contract;
    }

    @Provides
    @PerActivity
    MainActivityViewContract provideMainActivityViewContract(){
        return contract;
    }
}
