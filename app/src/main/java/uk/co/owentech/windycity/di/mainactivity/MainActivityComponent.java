package uk.co.owentech.windycity.di.mainactivity;

import dagger.Component;
import uk.co.owentech.windycity.di.PerActivity;
import uk.co.owentech.windycity.di.application.ApplicationComponent;
import uk.co.owentech.windycity.ui.main.MainActivity;

@PerActivity
@Component(modules = {MainActivityModule.class}, dependencies = ApplicationComponent.class)
public interface MainActivityComponent {
    void inject(MainActivity activity);
}
